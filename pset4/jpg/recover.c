/****************************************************************************
 * recover.c
 *
 * Computer Science 50
 * Problem Set 4
 *
 * Recovers JPEGs from a forensic image.
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

typedef uint8_t  BYTE;

int main(void)
{   
    
    int found_count = 0;
    FILE* out_fp = NULL;
    char file_name[4] = {"    "};
    
    // Open the card file
    FILE* fp = fopen("card.raw", "r");

    while (true)
    {
        // Read 512 bytes into a buffer
        BYTE buffer[512];
        fread(&buffer, sizeof(buffer), 1, fp);
        // If end of file is found
        if (feof(fp))
        {
            //Reached the end of the file
            fclose(fp);
            //close any open output files as well
            return 0;
        }

        // If start of a jpg
        if (buffer[0] == 0xff && buffer[1] == 0xd8 && buffer[2] == 0xff && (buffer[3] == 0xe0 || buffer[3] == 0x0e1))
        {
            //Close existing file
            if (out_fp != NULL)
            {
                fclose(out_fp);
                out_fp = NULL;
            }
            //Set up new file name
            
            sprintf(file_name, "%03d.jpg", found_count);
            found_count++;
            
            // Write the bytes into a file.
            if((out_fp = fopen(file_name, "w")) == NULL)
            {
                printf("Could not open new jpeg to write too.\n");
                return 1;
            }
            fwrite(&buffer, sizeof(BYTE), 512, out_fp);
        }
        else if (out_fp != NULL)
        {
            // Write bytes to the file
            fwrite(&buffer, sizeof(BYTE), 512, out_fp);
        }
    }

}
