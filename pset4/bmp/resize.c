/****************************************************************************
 * resize.c
 *
 * Thomas Shipley
 * Computer Science 50
 * Problem Set 4
 *
 * Resizes a bitmap image by a factor of n.
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "bmp.h"

bool is_bmp(BITMAPFILEHEADER* bfheader, BITMAPINFOHEADER* biheader);

int main(int argc, char* argv[])
{
    // ensure proper usage
    if (argc != 4)
    {
        printf("Usage: resize n infile outfile\n");
        return 1;
    }

    // get resize factor
    int factor = 0;
    if (sscanf(argv[1], " %d %c", &factor) != 1)
    {
        return 2;
    }

    // make sure resize factor between 1 - 100
    if (factor < 0 || factor > 100)
    {
        printf("Only resize factors 0 - 100 are supported.\n");
        return 6;
    }

    // remember filenames
    char* infile = argv[2];
    char* outfile = argv[3];

    // open input file
    FILE* inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        printf("Could not open %s.\n", infile);
        return 3;
    }

    // open output file
    FILE* outptr = fopen(outfile, "w");
    if (outptr == NULL)
    {
        fclose(inptr);
        fprintf(stderr, "Could not create %s.\n", outfile);
        return 4;
    }

    // read infile's BITMAPFILEHEADER
    BITMAPFILEHEADER bf;
    fread(&bf, sizeof(BITMAPFILEHEADER), 1, inptr);

    // read infile's BITMAPINFOHEADER
    BITMAPINFOHEADER bi;
    fread(&bi, sizeof(BITMAPINFOHEADER), 1, inptr);

    // check in file is a bmp
    if (!is_bmp(&bf, &bi))
    {
        fclose(outptr);
        fclose(inptr);
        fprintf(stderr, "Unsupported file format.\n");
        return 4;
    }

    //********************************************************
    //Make changes here to FILE & INFOHEADER to make correct *
    //size, width, height                                    *
    //********************************************************
    //take copies of the info and file header for the new image
    BITMAPFILEHEADER out_bf;
    BITMAPINFOHEADER out_bi;
    out_bf = bf;
    out_bi = bi;

    // calculate the out height and width
    out_bi.biHeight = bi.biHeight * factor;
    out_bi.biWidth = bi.biWidth * factor;

    // determine padding for scanlines
    int in_padding = (4 - (bi.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;
    int out_padding = (4 - (out_bi.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;

    // calulate the biSizeImage (bytes required) and bfSize(file size)
    out_bi.biSizeImage = ((out_bi.biWidth * 3) + out_padding) * abs(out_bi.biHeight);
    out_bf.bfSize = (out_bi.biSizeImage) + sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);

    // write outfile's BITMAPFILEHEADER
    fwrite(&out_bf, sizeof(BITMAPFILEHEADER), 1, outptr);

    // write outfile's BITMAPINFOHEADER
    fwrite(&out_bi, sizeof(BITMAPINFOHEADER), 1, outptr);

    // iterate over infile's scanlines
    for (int i = 0, biHeight = abs(bi.biHeight); i < biHeight; i++)
    {
        // add number of lines defined by the factor
        for (int x = 0; x < factor; x++)
        {
            //Move to the start of the correct line
            //Thanks to CuriousKiwi for the hints that got me to this.
            fseek(inptr, 54 + (((bi.biWidth * 3) + in_padding) * i), SEEK_SET);
            // iterate over pixels in scanline
            for (int j = 0; j < bi.biWidth; j++)
            {
                // temporary storage
                RGBTRIPLE triple;

                // read RGB triple from infile
                fread(&triple, sizeof(RGBTRIPLE), 1, inptr);

                // write RGB triple to outfile
                // write each pixal by the number of times defined by the factor
                for (int y = 0; y < factor; y++)
                {
                    fwrite(&triple, sizeof(RGBTRIPLE), 1, outptr);
                }
            }

            // skip over padding in the in_file
            fseek(inptr, in_padding, SEEK_CUR);

            // add padding to the out_file
            for (int k = 0; k < out_padding; k++)
            {
                fputc(0x00, outptr);
            }
        }
    }

    // close infile
    fclose(inptr);

    // close outfile
    fclose(outptr);

    // that's all folks
    return 0;
}

// Checks that a file is actually a bmp
bool is_bmp(BITMAPFILEHEADER* bfheader, BITMAPINFOHEADER* biheader)
{
    BITMAPFILEHEADER bf = *bfheader;
    BITMAPINFOHEADER bi = *biheader;

    if (bf.bfType != 0x4d42 || bf.bfOffBits != 54 || bi.biSize != 40 ||
        bi.biBitCount != 24 || bi.biCompression != 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}
