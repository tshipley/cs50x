/****************************************************************************
 * puff.c
 *
 * Computer Science 50
 * Problem Set 6
 * Thomas Shipley
 *
 * Decompresses a huffman tree file to the original text file.
 ***************************************************************************/

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "forest.h"
#include "huffile.h"
#include "tree.h"

// the root of the tree
Tree* tree_root = NULL;

int main(int argc, char* argv[])
{
    // ensure proper usage
    // huffman file and output txt file
    if (argc != 3)
    {
        printf("Usage: %s input output\n", argv[0]);
        return 1;
    }

    // open input
    Huffile* input = hfopen(argv[1], "r");
    if (input == NULL)
    {
        printf("Could not open %s for reading.\n", argv[1]);
        return 1;
    }

    // read in header
    Huffeader header;
    if (hread(&header, input) == false)
    {
        hfclose(input);
        printf("Could not read header.\n");
        return 1;
    }

    // check for magic number
    if (header.magic != MAGIC)
    {
        hfclose(input);
        printf("File was not huffed.\n");
        return 1;
    }

    // check checksum
    int checksum = header.checksum;
    for (int i = 0; i < SYMBOLS; i++)
    {
        checksum -= header.frequencies[i];
    }
    if (checksum != 0)
    {
        hfclose(input);
        printf("File was not huffed.\n");
        return 1;
    }

    //1. Create A Forest
    Forest* forest = mkforest();

    //2. Create a tree for each non 0 frequency char
    int tree_count = 0;
    for (int i = 0; i < SYMBOLS; i++)
    {
        if (header.frequencies[i] > 0)
        {
            //Create a tree
            Tree* tree = mktree();
            tree -> symbol = i;
            tree -> frequency = header.frequencies[i];
            tree_count++;
            //Plant the tree in the forest
            if (!plant(forest, tree))
            {
                //Error, fail gracefully
                if (!rmforest(forest))
                {
                    printf("Sorry there has been an error and unable to free memory while initializing trees.\n");
                    return 1;
                }

                printf("Sorry there has been an error while initializing trees.\n");
                return 1;
            }
        }
    }
    
    //3. Check if there is only one tree just print it to file and exit.
    if(tree_count == 1)
    {
        Tree* single_tree = pick(forest);
        FILE* singleout = NULL;
        singleout = fopen(argv[2], "w");
        fprintf(singleout, "%c", single_tree -> symbol);
        
        //Close files
        fclose(singleout);
        hfclose(input);
        
        //Free Forest
        if (!rmforest(forest))
        {
            //Failed to remove the forest
            printf("Sorry there has been an error while removing the tree at the end of the program.\n");
            return 1;
        }    

        return 0;
    }

    //4. Join lowest frequency trees until only one tree in forest.
    bool single_tree = false;
    while (!single_tree)
    {
        // Pick the two lowest frequency trees
        Tree* left_leaf = pick(forest);
        Tree* right_leaf = pick(forest);

        // Check there are actually two trees
        if (right_leaf == NULL)
        {
            tree_root = left_leaf;
            single_tree = true;
        }
        else
        {
            // Create a new parent tree
            Tree* parent = mktree();

            // Initialize the parent tree values
            parent -> frequency = ((left_leaf -> frequency) + (right_leaf -> frequency));
            parent -> left = left_leaf;
            parent -> right = right_leaf;

            // Plant the tree
            if (!plant(forest, parent))
            {
                //Error, fail gracefully
                if (!rmforest(forest))
                {
                    printf("Sorry there has been an error and unable to free memory while creating a parent tree.\n");
                    return 1;
                }

                printf("Sorry there has been an error while creating a parent tree.\n");
                return 1;
            }
        }
    }

    // Read the file bits and map to the tree
    int bit;
    Tree* position = tree_root;
    FILE* output = NULL;
    output = fopen(argv[2], "w");

    if (output == NULL)
    {
        // Could not open file
        printf("Could not open the file");
        hfclose(input);

        // Free all the memory
        rmtree(position);
        if (!rmforest(forest))
        {
            //Failed to remove the forest
            printf("Sorry there has been an error while removing the tree at the end of the program.\n");
            return 1;
        }
        
        return 1;
    }

    while ((bit = bread(input)) != EOF)
    {
        //Assume 0 == left leaf (per the spec)
        if (bit == 0)
        {
            position = position -> left;
        }
        //Assume 1 == right leaf (per the spec)
        else if (bit == 1)
        {
            position = position -> right;
        }

        //Check if you have found a char
        if ((position -> left == NULL) && (position -> right == NULL))
        {
            //Char found! Print to output file
            fprintf(output, "%c", position -> symbol);

            //Move back to the top of the tree
            position = tree_root;
        }
    }

    //Close file
    fclose(output);
    hfclose(input);

    // Free all the memory
    rmtree(position);
    if (!rmforest(forest))
    {
        //Failed to remove the forest
        printf("Sorry there has been an error while removing the tree at the end of the program.\n");
        return 1;
    }

    // that's all folks!
    return 0;
}
