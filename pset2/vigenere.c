/******************************************
* vingenere.c
*
* Computer Science 50
* Thomas Shipley
*
* Encrypting plain text using a vingenere
* cypher, key and string inputted by the
* user.
*
*******************************************/

#include <stdio.h>
#include <ctype.h>
#include <cs50.h>
#include <stdlib.h>
#include <string.h>

void encrypt(string phrase, string message);

int main(int argc, string argv[])
{
	// Check that the user has given the correct inputs before begining
	if(argc > 2)
	{
		printf("Please only enter your key as a string when running the program.\n");
		return 1;
	}
	else if(argc < 2)
	{
		printf("Please _enter your key as a string when running the program.\n");
		return 1;
	}
	
	string key_phrase = argv[1];
	
	// Check key phrase has no numbers in it
	for(int i = 0, n = strlen(key_phrase); i < n; i++)
	{
	    if(isdigit((int)key_phrase[i]))
	    {
	        printf("Please enter a key with just chars.");
	        return 1;
	    }
	}
	
	// Ask for the string to encrypt from the user
	string message_decrypted = GetString();
	
	//Perform encryption
	encrypt(key_phrase, message_decrypted);

	return 0; 
}

void encrypt(string phrase, string message)
{
    int key_length = strlen(phrase);
    // Track position in the key
	int key_position = 0;
	
	// Iterate through each letter of the decrypted string
	for(int i = 0, n = strlen(message); i < n; i++)
	{
		// Check to see if the key position needs to be reset
		if(key_position >= key_length)
		{
			key_position = 0;
		}
		
		// Get the letter to decrypt and the letter of the key
		char letter = message[i];
		char key_letter = phrase[key_position];
		int cipher_key = 0;
		if(isupper((int)key_letter))
		{
		    cipher_key = ((int)key_letter - 'A'); //- 65
		}
		else
		{
		    cipher_key = ((int)key_letter - 'a'); // - 97
		}
		// If a aplha then do encryption on the char
		if(isalpha((int)letter))
		{
			// Uppercase Logic
			if(isupper((int)letter))
			{
				if((int)letter + cipher_key >= 'Z')
				{
				    int diff = ((int)letter + cipher_key) % 'Z';
					letter = 'A' + (diff - 1);
				}
				else
				{
					letter = (int)letter + cipher_key;
				}

				printf("%c", letter);
			}
			else if(islower((int)letter))
			{
				// This is a lower case char
				if((int)letter + cipher_key >= 'z')
				{
				    int diff = ((int)letter + cipher_key) % 'z';
					letter = 'a' + (diff - 1);
				}
				else
				{
					letter = (int)letter + cipher_key % 26;
				}
                
				printf("%c", letter);
			}
			else
			{
                if(key_position != 0)
                {
                    key_position--;
                }
                printf("%c", letter); 
			}
		}
		else
		{
			if(key_position != 0)
			{
				// Need to reset the key position to reuse this key char
				key_position--;
			}
			printf("%c", letter);
		}
		key_position++;
	}

	// Print newline to finish
	printf("\n");
}
