/***************************************************
* caesar.c
*
* Computer Science 50
* Thomas Shipley
*
* Takes a inputed key to shift the chars of a string
* by.
****************************************************/

#include <stdio.h>
#include <ctype.h>
#include <cs50.h>
#include <stdlib.h>
#include <string.h>

void printEncrypted(string decrypted, int key);

int main(int argc, string argv[])
{
    // Check the inputted argument is correct
    if(argc > 2)
    {
        printf("Too many arguments, only enter the cipher key as an argument.\n");
        return 1;
    }
    else if (argc < 2)
    {
        printf("Please input the cipher key as a argument.\n");
        return 1;
    }
    
    // Get the key and decrypted string
    int cipher_key = atoi(argv[1]) % 26;
    string string_decrypted = GetString();
    
    // Print to console the encrypted string
    printEncrypted(string_decrypted, cipher_key);
    printf("\n");
    return 0;
}

void printEncrypted(string decrypted, int key)
{
    for (int i = 0, n = strlen(decrypted); i < n; i++)
    {
        char x = decrypted[i];
        if(isalpha((int)x))
        {
            //Upper case encryption logic
            if(isupper((int)x))
            {

                if((int)x + key >= 'Z')
                {
                    int diff = ((int)x + key) % 'Z';
                    x = 'A' + (diff - 1);
                }
                else
                {
                    x = (int)x + key % 26;
                }
            }
            //Lower case encryption logic
            if(islower((int)x))
            {
                if((int)x + key >= 'z')
                {
                    int diff = ((int)x + key) % 'z';
                    x = 'a' + (diff - 1);
                }
                else
                {
                    x = (int)x + key;
                }
            }
        }
        
        printf("%c", x);
    }
}
