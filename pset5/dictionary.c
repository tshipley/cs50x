/****************************************************************************
 * dictionary.c
 *
 * Computer Science 50
 * Problem Set 5
 * Thomas Shipley
 *
 * Implements a dictionary's functionality.
 ***************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "dictionary.h"

#define HASHTABLE_SIZE 1024

// Define node type
typedef struct node
{
    char value[LENGTH + 1];
    struct node* next;
} node;

int HASHTABLE_COUNT = 0;

// Define hashtable
node* HASHTABLE[HASHTABLE_SIZE];

/**
* Creates the hash value for the word
*/
int hash(const char* hash_word);

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char* word)
{
    //Copy of the word as it is read only so cannot make changes (to lower)
    char word_copy[LENGTH + 1];

    //Convert the word to lower to compare against dictionary
    int size = strlen(word);
    for (int i = 0; i < size; i++)
    {
        int lower_value = tolower(word[i]);
        word_copy[i] = (char)lower_value;
    }
    // Must mark end of string otherwise horribleness - don't assume the loop does it.
    word_copy[size] = '\0';

    //Get the hash for the word
    int hash_value = hash(word_copy);

    //Check there is a node there
    if (HASHTABLE[hash_value] == NULL)
    {
        return false;
    }

    //Get that node pointer
    node* node = HASHTABLE[hash_value];

    while (node != NULL)
    {
        //Check if the words match if so return true
        if (strcmp(word_copy, node -> value) == 0)
        {
            return true;
        }

        //If no match then go to the next node
        node = node -> next;
    }
    return false;
}

/**
 * Loads dictionary into memory.  Returns true if successful else false.
 */
bool load(const char* dictionary)
{
    // Open the dictionary file
    FILE* fp = NULL;
    fp = fopen(dictionary, "r");

    if (fp == NULL)
    {
        printf("Opening the dictionary file failed.");
        return false;
    }

    //Init the hashtable
    for (int i = 0; i < HASHTABLE_SIZE; i++)
    {
        HASHTABLE[i] = NULL;
    }

    char current_word[LENGTH + 1];

    // Iterate over the dictionary word
    while (true)
    {
        //Get a word from the dictionary
        int result = fscanf(fp, "%s\n", current_word);

        if (result < 1)
        {
            fclose(fp);
            return true;
        }

        //Create the new node
        node* new_node = malloc(sizeof(node));
        new_node -> next = NULL;

        //Add word to the node
        strcpy(new_node -> value, current_word);
        //printf("The node word value is:'%s'\n", new_node -> value);
        //Get the hash value
        int hashvalue = hash(new_node -> value);

        if (HASHTABLE[hashvalue] == NULL)
        {
            // Insert the node pointer into the hashtable
            HASHTABLE[hashvalue] = new_node;

            // Increment the number of words in the dictonary
            HASHTABLE_COUNT++;
        }
        else
        {
            //Handle a collision
            //Create a new node pointing to the existing node
            new_node -> next = HASHTABLE[hashvalue];

            //Point the hash table to the new node
            HASHTABLE[hashvalue] = new_node;

            // Increment the number of words in the dictonary
            HASHTABLE_COUNT++;
        }
    }
    return false;
}

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void)
{
    // Return the count of words in the hashtable
    return HASHTABLE_COUNT;
}

/**
 * Unloads dictionary from memory.  Returns true if successful else false.
 */
bool unload(void)
{
    //Create two nodes to pass between
    node* node_a = NULL;
    node* node_b = NULL;

    //For each element in hashtable
    for (int i = 0; i < HASHTABLE_SIZE; i++)
    {
        //Set node_a to the first element in hashtable location
        node_a = HASHTABLE[i];

        //While node_a is not null (could be null at start)
        while (node_a != NULL)
        {
            //Set node_b to next node in list
            node_b = node_a -> next;

            //Free nodes malloced values
            free(node_a);

            //Set node_a to next node in list
            node_a = node_b;
        }
    }
    return true;
}

/**
 * Creates the hash value for a given word
 */
int hash(const char* hash_word)
{
    int result = 0;
    int n = strlen(hash_word);

    for (int i = 0; i < n; i++)
    {
        result += (hash_word[i] * i) - i;
    }
    return result % HASHTABLE_SIZE;
}
