/*
* hello.c
*
* Computer Science 50
* Thomas Shipley
*
* Prints hello world to the console
*/

#include <stdio.h>

/*
* Print hello world to console and return 0
*/

int main(void)
{
    printf("hello, world\n");
    return 0;
}
