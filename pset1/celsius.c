/*
* celsius.c
*
* Computer Science 50
* Thomas Shipley
*
* Converts a temperature from Celsius to Fahrenheit
*
* Demonstrates usage of printf and scanf
*/

#include <stdio.h>

float Celsius_To_Fahrenheit(int celsius_Converting);

/*
* Captures inputed celsius_Temp from the user.
* Prints the converted fahrenheit temperature to the user.
*/
int main(void)
{
    int celsius_Temp = 0;
    float fahrenheit_Temp;
    
    //Capture the celsius temp to convert from user.
    printf("C: ");
    scanf("%i", &celsius_Temp);
    fahrenheit_Temp = Celsius_To_Fahrenheit(celsius_Temp);
    
    //Display converted celsius temp to fahrenheit temp.
    printf("F: %.1f\n", fahrenheit_Temp);
    return 0;
}

/*
* Returns the inputed celsius temperature converted into
* fahrenheit.
*/
float Celsius_To_Fahrenheit(int celsius_Converting)
{
    return celsius_Converting * 9 / 5 + 32;
}
