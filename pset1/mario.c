/*
* mario.c
*
* Computer Science 50
* Thomas Shipley
*
* Takes an inputed height value from the user between 1 and 23
* and builds a pryamid by writing '#'s to the console window
* which are right aligned.
*/

#include <stdio.h>
#include <cs50.h>

void build_pryamid(int height);

/*
* Gets height from user to build
* pryamid to.
*/

int main(void)
{
    // Get the height of the pryamid
    printf("Height: ");
    int pryamid_height = GetInt();

    // Check height entered between 1 and 23 if not ask again
    while (pryamid_height <= 0 || pryamid_height > 23)
    {
        // If user enters 0 exit program
        if (pryamid_height == 0)
            return 0; 

        printf("Retry: ");
        pryamid_height = GetInt();
    }
    
    // Build the pryamid to height user entered
    build_pryamid(pryamid_height);

    return 0;
}

/*
* Builds pryamid using '#' for blocks
* into the console window to the
* height inputted.
*/

void build_pryamid(int height)
{
    //First row width always two blocks
    int row_width = 2;
    
    //Build pryamid row by row.
    for (int x = 0; x != height; x++)
    {
        //Manipulate precision/width format to get row spaces / '#' mix
        printf("%*s", height - (x + 1), "");
        printf("%.*s\n", row_width, "########################");
        row_width++;
    }
}
