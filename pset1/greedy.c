/*
* greedy.c
*
* Computer Science 50
* Thomas Shipley
*
* Takes a inputed float value for dollars and counts the
* least amount of coins needed to give the change.
*/

#include <stdio.h>
#include <math.h>
#include <cs50.h>

// Define coin values
#define cents_in_quarter 25
#define cents_in_dime 10
#define cents_in_nickels 5

int change_coin_count(int total_change);

/*
* Gets the amount in dollars to count the
* min amount of coins needed in change.
*/

int main(void)
{
    // Get change as dollars
    printf("O hai! How much change is owed?\n");
    float change_as_dollars = GetFloat();
    
    // Check not a negative amount
    while( change_as_dollars < 0.0f )
    {
        printf("How much change is owed?\n");
        change_as_dollars = GetFloat();
    }
    
    // Convert to coin amount
    int change_as_cents = (int)round(( change_as_dollars * 100 ));
  
    // Count coins needed in change
    int coin_count = change_coin_count(change_as_cents);
    
    printf("%i\n", coin_count);
    
    return 0;
}

/*
* Counts the min amount of coins needed
* in change for the inputed change total.
*/

int change_coin_count(int total_change)
{
    int count = 0;
    
    // Use while loops to test change left in assending order
    while( total_change >= cents_in_quarter )
    {
        count++;
        total_change -= cents_in_quarter;
    }

    while( total_change >= cents_in_dime )
    {
        count++;
        total_change -= cents_in_dime;
    }

    while( total_change >= cents_in_nickels )
    {
        count++;
        total_change -= cents_in_nickels;
    }

    // Only pennies left so add to coin count 
    count += total_change;
    
    return count;
}
